# GetThoseMicrosoftRewardsSon
Drop these files into the same directory.

These run from Git Bash on Windows 11 and they work well. WSL should work too.

I don't recall where I found the list of words. I will add attribution if the source is found.

## Prerequisites
Your Edge browser should automatically sign in to your microsoft account.

## Usage
Simply cd to the directory you copied these files to and run each one, closing your browser between each run.

I put my files in ~/bin

```
cd ~/bin; ./ms_points.sh
```
This is a wrapper script that will run edge.sh and mobile_edge.sh.

**edge.sh**

This will ouput 34 search words. Once in awhile some errors from Edge appear on the console as well. You can ignore them.

The browser window will close and a new window will open that emulates a mobile browser.

**mobile_edge.sh** 

This will output 20 search words. Once in awhile some errors from Edge appear on the console as well. You can ignore them.

Once run you should have 150 PC points, 20 Edge points and 100 mobile points.

If the points aren't maxed out, run the appropriate script again. Every once in awhile a search won't register, I am not sure why.

## Sample output
```
shawn@TI994a MINGW64 ~/bin
$ cd ~/bin; ./ms_points.sh
miscited - 1
[5996:1152:0616/131316.602:ERROR:chrome_browser_cloud_management_controller.cc(162)] Cloud management controller initialization aborted as CBCM is not enabled.
[5996:1152:0616/131318.846:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[5996:1152:0616/131318.885:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[5996:1152:0616/131318.887:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[5996:1152:0616/131318.900:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[5996:1152:0616/131318.905:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[5996:1152:0616/131318.924:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
rippled - 2
[5996:22956:0616/131320.819:ERROR:cert_issuer_source_aia.cc(34)] Error parsing cert retrieved from AIA (as DER):
ERROR: Couldn't read tbsCertificate as SEQUENCE
ERROR: Failed parsing Certificate

[5996:1152:0616/131321.108:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: f4abcb92-18d0-4178-ac71-3ef6c27b7136
[5996:1152:0616/131321.109:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: ffeba9c0-25c3-4fe7-a1db-5ce22948d876
[5996:19908:0616/131321.322:ERROR:cert_issuer_source_aia.cc(34)] Error parsing cert retrieved from AIA (as DER):
ERROR: Couldn't read tbsCertificate as SEQUENCE
ERROR: Failed parsing Certificate

[5996:22956:0616/131321.481:ERROR:cert_issuer_source_aia.cc(34)] Error parsing cert retrieved from AIA (as DER):
ERROR: Couldn't read tbsCertificate as SEQUENCE
ERROR: Failed parsing Certificate

carving - 3
[5996:1152:0616/131322.673:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
palace - 4
zygose - 5
[5996:1152:0616/131330.405:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: e740d7f0-8ef2-4681-9583-0b42042af029
[5996:1152:0616/131330.405:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 9b6b27ca-17d3-4755-b139-8930e3dba882
[5996:1152:0616/131330.777:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
macrames - 6
ammonification - 7
haymakers - 8
inanenesses - 9
discussers - 10
inanition - 11
citadels - 12
speedboats - 13
dewooling - 14
spurred - 15
lapped - 16
yep - 17
moulin - 18
osteoarthritis - 19
kiosks - 20
reacquiring - 21
chorally - 22
rectal - 23
chenopods - 24
farcies - 25
bulkhead - 26
airbags - 27
phytogeography - 28
humanised - 29
episodes - 30
parqueted - 31
refuels - 32
preemptive - 33
bipropellants - 34
fettles - 1
[21668:12040:0616/131501.792:ERROR:chrome_browser_cloud_management_controller.cc(162)] Cloud management controller initialization aborted as CBCM is not enabled.
[21668:12040:0616/131504.329:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[21668:12040:0616/131504.337:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[21668:12040:0616/131504.347:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[21668:12040:0616/131504.359:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
[21668:12040:0616/131504.563:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
spelt - 2
Opening in existing browser session.
[21668:27732:0616/131506.213:ERROR:cert_issuer_source_aia.cc(34)] Error parsing cert retrieved from AIA (as DER):
ERROR: Couldn't read tbsCertificate as SEQUENCE
ERROR: Failed parsing Certificate

[21668:6884:0616/131506.678:ERROR:cert_issuer_source_aia.cc(34)] Error parsing cert retrieved from AIA (as DER):
ERROR: Couldn't read tbsCertificate as SEQUENCE
ERROR: Failed parsing Certificate

[21668:23892:0616/131506.678:ERROR:cert_issuer_source_aia.cc(34)] Error parsing cert retrieved from AIA (as DER):
ERROR: Couldn't read tbsCertificate as SEQUENCE
ERROR: Failed parsing Certificate

[21668:17524:0616/131506.678:ERROR:cert_issuer_source_aia.cc(34)] Error parsing cert retrieved from AIA (as DER):
ERROR: Couldn't read tbsCertificate as SEQUENCE
ERROR: Failed parsing Certificate

microtomy - 3
Opening in existing browser session.
[21668:12040:0616/131508.626:ERROR:fallback_task_provider.cc(124)] Every renderer should have at least one task provided by a primary task provider. If a "Renderer" fallback task is shown, it is a bug. If you have repro steps, please file a new bug and tag it as a dependency of crbug.com/739782.
nonhomogeneous - 4
Opening in existing browser session.
corselettes - 5
Opening in existing browser session.
bursa - 6
Opening in existing browser session.
tyres - 7
Opening in existing browser session.
wreakers - 8
Opening in existing browser session.
subjoining - 9
Opening in existing browser session.
apicultural - 10
Opening in existing browser session.
bejumbles - 11
Opening in existing browser session.
rubythroat - 12
Opening in existing browser session.
washable - 13
Opening in existing browser session.
aerosols - 14
Opening in existing browser session.
[21668:12040:0616/131541.581:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 7fd5272a-49bf-49d8-96da-d4f56cb12ef6
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: ce57c08b-617c-404f-9876-4e94a6e19c13
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: c1a426f2-d2ec-4e6f-9281-5e8e2acec55a
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 96ffaf01-c24e-4081-bf4f-97a3198a9353
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 27548036-1436-44a5-80c2-df2e00469e65
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 3c973606-f092-4365-b6de-b39b0a7e6d5b
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 1b15d928-d3b8-4773-b118-9f039714710e
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 95e1298c-326e-4a27-966c-fbf827a57774
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 306a6cb7-4079-44e6-99cb-bdf3f85d4445
[21668:12040:0616/131541.582:ERROR:processor_entity.cc(172)] Metadata has an exisiting server id that will be overwritten with the current update entity id.metadata server_id:  update.entity.id: 31fa018f-128f-4356-9974-cc39aea0900f
airfreight - 15
Opening in existing browser session.
ingenues - 16
Opening in existing browser session.
petrales - 17
Opening in existing browser session.
tetraploids - 18
Opening in existing browser session.
degenerateness - 19
Opening in existing browser session.
sinfoniettas - 20
Opening in existing browser session.

shawn@TI994a MINGW64 ~/bin
$
```
